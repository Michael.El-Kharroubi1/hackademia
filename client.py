import jwt
import requests
import urllib3
from models import StudentIdResponse, Student, ProofResponse, GradeAssignementRequest, GradeResponse
from random import choice, choices
from typing import Tuple
import string

urllib3.disable_warnings()

GREEN = "\033[92m"
RED = "\033[91m"
BASIC = "\033[0m"


def get_key() -> bytes:
    priv: bytes
    with open("priv_sign_key_verifier.pem", 'r') as f:
        priv = f.read()
    return priv


VERIF_PRIVKEY: bytes = get_key()


def sign_payload(data: str) -> str:
    return jwt.encode({"data": data}, VERIF_PRIVKEY, algorithm="RS512")


def print_student(s: Student):
    print(f"First name : {s.first_name}")
    print(f"Last name : {s.last_name}")
    print("Exam :")
    print(f"\tCode : {s.exam_code}")
    print(f"\tTitle : {s.exam_title}")
    print(f"\tStart : {s.exam_start.strftime('%d/%m/%Y at %H:%M')}")
    print(f"\tSeat : {s.seat}")
    print(f"\tPresent : {s.was_present}")
    print(f"\tSumbit : {s.has_submit}")
    print("Additionals :")
    for k, v in s.additionals.items():
        print(f"\t{k} : {v}")


def random_copy_id() -> str:
    return ''.join(choices(string.hexdigits[:16].upper(), k=8))


COPIES = [random_copy_id() for _ in range(1)]


def scan_copy() -> str:
    return choice(COPIES)


class Step:
    _server = ""
    _verify = False

    def __init__(self):
        self._server += "https://127.0.0.1:"
        self._verify = False


class Authentication(Step):
    def __init__(self):
        super().__init__()
        self._server += "8000"

    def authentify_with_card_id(self, card_id: str) -> StudentIdResponse:
        req = requests.post(f"{self._server}/getID",
                            json={"card_id": card_id},
                            verify=self._verify)
        return StudentIdResponse.parse_raw(req.text)

    def authentify_with_names(self, first_name: str, last_name: str) -> StudentIdResponse:
        req = requests.post(f"{self._server}/getID/fromName",
                            json={"first_name": first_name,
                                  "last_name": last_name},
                            verify=self._verify)
        return StudentIdResponse.parse_raw(req.text)

    def read_card(self) -> str:
        """
        Simulate a read from a nfc card or the qr code.
        """
        return '55A3'  # choice(['55A3', '8780', 'EE1D', 'BCA9', 'D5BB', '1E3C', '281F', 'EF74', '415E', '3C84'])

    def read_names(self) -> Tuple[str, str]:
        """
        Simulate a read from an id card from a verifier.
        """
        return ('Abdel', 'Nguyen')
        """choice([('Hakim', 'Dubois'), ('Marie', 'Sy'), ('Pierre', 'Sy'),
                       ('Marie', 'Yu'), ('Salma', 'Nguyen'),
                       ('Léa', 'Kante'), ('Marie', 'Kante'),
                       ('Pierre', 'Dubois'), ('Abdel', 'Nguyen'),
                       ('Marie', 'Dubois')])"""


class Verifier(Step):
    def __init__(self):
        super().__init__()
        self._server += "4269"

    def __send_update(self, student: Student, good: str, bad: str):
        encoded = sign_payload(student.json())
        req = requests.post(f"{self._server}/updateStudent",
                            json={"payload": encoded},
                            verify=self._verify)
        response: ProofResponse = ProofResponse.parse_raw(
            req.text)
        if response.status:
            print(f"{GREEN}{good}{BASIC}")
        else:
            print(f"{RED}{bad}{BASIC}")
        print_student(student)

    def verify_student(self, student_id: int):
        req = requests.post(f"{self._server}/getStudent",
                            json={"student_id": student_id},
                            verify=self._verify)
        student: Student = Student.parse_raw(req.text)
        print_student(student)
        verif = ""
        while verif not in ["yes", "no"]:
            verif = input("Everything is good (Yes or No)\n").lower()
        if verif == "yes":
            """Sign"""
            student.was_present = True
            student.additionals["copy_id"] = scan_copy()
        else:
            """Add commentary"""
            student.was_present = False

        comment = input("If you have any comments : ")
        if len(comment) > 0:
            student.additionals["comment at verification"] = comment

        self.__send_update(student, "Verification acknowledged",
                           "The verification process failed")

    def submit_exam(self, student_id: int):
        req = requests.post(f"{self._server}/getStudent",
                            json={"student_id": student_id},
                            verify=self._verify)
        student: Student = Student.parse_raw(req.text)
        if not student.was_present:
            print("The student was marked as absent")
        else:
            print_student(student)
            pages = int(input("How many pages : "))
            print("Please cut the coupon for the student")
            student.additionals["number of pages"] = pages
            comment = input("If you have any comments : ")
            if len(comment) > 0:
                student.additionals["comment at submission"] = comment
            student.has_submit = True
            self.__send_update(student, "The exam was submitted",
                               "The exam was submission failed")

    def check_student(self, student_id: int):
        req = requests.post(f"{self._server}/getStudent",
                            json={"student_id": student_id},
                            verify=self._verify)
        student: Student = Student.parse_raw(req.text)
        print_student(student)
        comment = input("If you have any comments : ")
        if len(comment) > 0:
            student.additionals["comment after check"] = comment
            self.__send_update(student, "The comment has been saved",
                               "The comment couldn't been saved")


class ExamCorrector(Step):
    def __init__(self):
        super().__init__()
        self._server += "4269"

    def correct_copy(self, copy_id: str):
        grade: float = float(
            input(f"Please enter the grade for copy {copy_id} : "))
        verif = ""
        while verif not in ["yes", "no"]:
            verif = input(
                f"The grade is {grade}.\nPlease type Yes or No\n").lower()
        if verif == "yes":
            encoded = sign_payload(GradeAssignementRequest(
                copy_id=copy_id.strip(), grade=grade).json())
            req = requests.post(f"{self._server}/assignGrade",
                                json={"payload": encoded},
                                verify=self._verify)
            response: GradeResponse = GradeResponse.parse_raw(req.text)
            if response.status:
                print(
                    f"{GREEN}The copy {response.grading[0]} was assigned the grade {response.grading[1]}{BASIC}")
            else:
                print(f"{RED}The grade couldn't be assigned{BASIC}")
