## To start bdd unige :

```bash
uvicorn bdd_unige:app --reload --port=8000 --ssl-keyfile key.pem --ssl-certfile cert.pem --ssl-keyfile-password Super
```

## To start bdd exam :

```bash
uvicorn bdd_exam:app --reload --port=4269 --ssl-keyfile key.pem --ssl-certfile cert.pem --ssl-keyfile-password Super
```

## Run client :

```bash
python client.py
```