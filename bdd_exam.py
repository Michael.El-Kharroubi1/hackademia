import random

from fastapi import FastAPI
from pydantic import BaseModel
from typing import List, Tuple, Dict
from models import StudentIdResponse, Student, random_ids, ProofResponse, GradeResponse, GradeAssignementRequest
from random import seed, choice, randint, choices
from datetime import datetime
import string
import jwt


class StudentIdRequest(BaseModel):
    student_id: int


class SignedRequest(BaseModel):
    payload: str


def random_exam() -> Tuple[str, str, datetime]:
    exam_code = "X" + ''.join(choices(string.hexdigits.upper()[:16], k=3))
    exam_title = choice(
        ["Analyse", "Algèbre", "Programmation", "Économie"]) + f" {randint(1, 9)}"
    exam_start = datetime(year=2022, month=choice(
        [2, 6]), day=randint(1, 28), hour=choice([8, 10, 13, 15]))
    return exam_code, exam_title, exam_start


def random_student(student_id: int, first_name: str, last_name: str) -> Student:
    exa = random_exam()
    return Student(student_id=student_id,
                   first_name=first_name,
                   last_name=last_name,
                   exam_code=exa[0],
                   exam_title=exa[1],
                   exam_start=exa[2],
                   seat=choice(string.ascii_uppercase) + str(randint(0, 9)),
                   was_present=False,
                   has_submit=False,
                   additionals={})


def get_key() -> bytes:
    pub: bytes
    with open("pub_sign_key_verifier.pem", 'r') as f:
        pub = f.read()
    return pub


VERIF_PUBKEY: bytes = get_key()


def verify(data: str) -> dict:
    return jwt.decode(data, VERIF_PUBKEY,
                      algorithms=["RS512"], options={"verify_signature": True})["data"]


fake_BDD = {s_id[0]: random_student(*s_id) for s_id in random_ids()}

app = FastAPI()


@app.post("/getStudent")
async def get_id(req: StudentIdRequest) -> Student:
    return fake_BDD.get(req.student_id, None)


@app.post("/updateStudent")
async def verif_student(req: SignedRequest) -> ProofResponse:
    try:
        stud: Student = Student.parse_raw(verify(req.payload))
        fake_BDD[stud.student_id] = stud
        return ProofResponse(status=True, student=fake_BDD[stud.student_id])
    except:
        return ProofResponse(status=False, student=None)


@app.post("/assignGrade")
async def assign_grade(req: SignedRequest) -> GradeResponse:
    try:
        grade_req: GradeAssignementRequest = GradeAssignementRequest.parse_raw(
            verify(req.payload))
        student_id: int = next(filter(lambda x: x.additionals.get('copy_id', "") == grade_req.copy_id,
                                      fake_BDD.values())).student_id
        print(student_id)
        fake_BDD[student_id].additionals['grade'] = grade_req.grade
        return GradeResponse(status=True, grading=(fake_BDD[student_id].additionals['copy_id'], fake_BDD[student_id].additionals['grade']))
    except:
        return GradeResponse(status=False)
