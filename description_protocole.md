# Protocole vérification examen (Hackademia)

## Authentification :

### Étapes clés (cas standard):

1. Récupération de l'ID étudiant depuis la carte (NFC / QR).
1. Récupération depuis la bdd uni de l'ID  uni étudiant à partir l'ID carte

### Étapes clés (cas sans carte unige):

1. Récupération du nom + prénom de l'étudiant depuis une carte d'identité.
2. Récupération depuis la bdd uni de l'ID  uni étudiant à partir du nom + prénom.
3. Si première fois - Remplissage de la bdd exam

## Vérification de présence (pré-requis : authentification) :

### Étapes clés:

1. Interrogation de la base de donnée exam avec l'ID étu.
2. Affichage des données de l'étudiant.
3. Vérification des données par le vérificateur.
   1. Si examens anonymes scan du QR de l'exa. 
4. Signature des données.
5. Envoie de validation signée par le vérificateur.
6. Confirmation de réception par le serveur.

## Consultation (pré-requis : authentification) :

### Étapes clés:

1. Interrogation de la base de donnée exam avec l'ID étu.
2. Affichage des données de l'étudiant.
   1. En cas de problème, commentaire du vérificateur.
   2. Signature des données.
   3. Envoie du commentaire signé par le vérificateur.
   4. Confirmation de réception par le serveur.

## Vérification rendu (pré-requis : authentification) :

### Étapes clés:

1. Interrogation de la base de donnée exam avec l'ID étu.
2. Validation entre le vérificateur et l'étudiant du nombre de pages rendu.
   1. Si examen anonyme découpage du coupon pour l'étudiant.
3. Validation du rendu de l'étudiant par le vérificateur.
4. Signature des données.
5. Envoie de validation signée par le vérificateur.
6. Côté serveur
   1. Génération d'un PV d'examen comprenant: matricule unige, nom, prénom, validation de présence, validation de rendu (Si exa anonyme, code exam).
   2. Signature numérique du PV par le serveur, et envoie par mail du pv.
7. Confirmation de réception par le serveur.

## Correction d'une copie

### Étapes clés:

1. Le correcteur scan le QR de la copie.
2. Le formulaire de rentrée de la note s'affiche.
3. Le correcteur signe le formulaire.
4. Le formulaire signé est envoyé.
5. Confirmation de réception par le serveur.