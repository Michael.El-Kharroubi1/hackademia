from client import Authentication, Verifier, ExamCorrector
from models import StudentIdResponse


def main():
    print("Vérification automatique et anonyme de la présence des étudiant-es lors d'un examen")
    print("Veuillez choisir une action :")
    print("1.) Vérification de la présence d'un étudiant (tapez verif)")
    print("2.) Consultation des informations de l'étudiant (tapez consult)")
    print("3.) Marquage du rendu de l'examen de l'étudiant (tapez rendu)")
    print("4.) Correction d'une copie (tapez corr)")
    print("0.) Arrêt du programme (tapez exit)")

    choice = ""

    while choice not in ["verif", "consult", "rendu", "corr", "exit"]:
        choice = input("Choix : ")

    auth = Authentication()
    verif = Verifier()
    corrector = ExamCorrector()

    student: StudentIdResponse = auth.authentify_with_card_id(auth.read_card())
    if choice == "verif":
        input("Pour scanner la carte de l'étudiant, tapez sur entrée")
        verif.verify_student(student.student_id)
    elif choice == "consult":
        input("Pour scanner la carte de l'étudiant, tapez sur entrée")
        verif.check_student(student.student_id)
    elif choice == "rendu":
        input("Pour scanner la carte de l'étudiant, tapez sur entrée")
        verif.submit_exam(student.student_id)
    elif choice == "corr":
        corrector.correct_copy(input("Copy : "))
    else:
        print("Au revoir et bientôt à l'UNIGE!!!!!!!")
        exit(0)


if __name__ == '__main__':
    main()