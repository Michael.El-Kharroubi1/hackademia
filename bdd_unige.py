from fastapi import FastAPI
from pydantic import BaseModel
from typing import List
from models import StudentIdResponse, random_ids
from random import seed, randint, choices, choice
import string


class StudentUniDb(BaseModel):
    card_id: str
    student_id: int
    first_name: str
    last_name: str


class IdRequest(BaseModel):
    first_name: str
    last_name: str


class CardIdRequest(BaseModel):
    card_id: str


def random_student(student_id: int, first_name: str, last_name: str) -> StudentUniDb:
    return StudentUniDb(card_id=''.join(choices(string.hexdigits.upper()[:16], k=4)),
                        student_id=student_id,
                        first_name=first_name,
                        last_name=last_name)


fake_BDD = [random_student(*s_id) for s_id in random_ids()]

app = FastAPI()


@app.post("/getID")
async def get_id(req: CardIdRequest) -> StudentIdResponse:
    tmp: StudentUniDb = next(
        filter(lambda x: x.card_id == req.card_id, fake_BDD), -1)
    return StudentIdResponse(student_id=tmp.student_id)


@app.post("/getID/fromName")
async def get_id(req: IdRequest) -> StudentIdResponse:
    tmp: int = next(filter(lambda x: x.first_name == req.first_name
                           and x.last_name == req.last_name, fake_BDD), -1)
    return StudentIdResponse(student_id=tmp.student_id)
