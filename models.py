from pydantic import BaseModel
from typing import Optional, Dict, Any, Tuple, Union
from datetime import datetime
from random import seed, randint, choice


class Student(BaseModel):
    student_id: int
    first_name: str
    last_name: str
    exam_code: str
    exam_title: str
    exam_start: datetime
    seat: str
    was_present: bool
    has_submit: bool
    additionals: Dict[str, Any]


class StudentIdResponse(BaseModel):
    student_id: int


class ProofResponse(BaseModel):
    status: bool
    student: Union[Student, None]


class GradeResponse(BaseModel):
    status: bool
    grading: Union[Tuple[str, float], None]


class GradeAssignementRequest(BaseModel):
    copy_id: str
    grade: float


def random_ids() -> Tuple[int, str, str]:
    seed(0)

    return [tuple([randint(10000000, 100000000-1),
                  choice(["Léa", "Salma", "Pierre", "Hakim", "Marie", "Abdel"]),
                  choice(["Dubois", "Yu", "Nguyen", "Sy", "Kante"])]) for _ in range(10)]
